Source: golang-github-google-wire
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-exec,
               dh-sequence-golang,
               golang-any (>= 2:1.17~),
               golang-github-google-go-cmp-dev (>= 0.2.0),
               golang-github-google-subcommands-dev (>= 1.0.1),
               golang-github-pmezard-go-difflib-dev (>= 1.0.0),
               golang-golang-x-tools-dev (>= 1:0.0~git20190422.fe54fb3)
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-google-wire
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-google-wire.git
Homepage: https://github.com/google/wire
XS-Go-Import-Path: github.com/google/wire

Package: golang-github-google-wire-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-google-go-cmp-dev (>= 0.2.0),
         golang-github-google-subcommands-dev (>= 1.0.1),
         golang-github-pmezard-go-difflib-dev (>= 1.0.0),
         golang-golang-x-tools-dev (>= 1:0.0~git20190422.fe54fb3),
         ${misc:Depends}
Description: Compile-time Dependency Injection for Go (library)
 Wire: Automated Initialization in Go
 .
 Wire is a code generation tool that automates connecting components
 using dependency injection.  Dependencies between components are
 represented in Wire as function parameters, encouraging explicit
 initialization instead of global variables.  Because Wire operates
 without runtime state or reflection, code written to be used with
 Wire is useful even for hand-written initialization.
 .
 For an overview, see the introductory blog post
 https://blog.golang.org/wire
 .
 This package contains the github.com/google/wire Go library.

Package: google-wire
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Compile-time Dependency Injection for Go (program)
 Wire: Automated Initialization in Go
 .
 Wire is a code generation tool that automates connecting components
 using dependency injection.  Dependencies between components are
 represented in Wire as function parameters, encouraging explicit
 initialization instead of global variables.  Because Wire operates
 without runtime state or reflection, code written to be used with
 Wire is useful even for hand-written initialization.
 .
 For an overview, see the introductory blog post
 https://blog.golang.org/wire
 .
 This package contains the wire command-line program.
